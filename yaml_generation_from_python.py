import kfp
from kfp.components import func_to_container_op, InputPath, OutputPath,create_component_from_func
from kfp import dsl 
from kubernetes import client as k8s_client
import pickle
from typing import NamedTuple
from Pipeline_Components.Input_Parameters.input import input_parameters
from Pipeline_Components.Model_Parameters.model_parameters import model_parameters
from Pipeline_Components.Model_Setup.model_setup import model_setup
from Pipeline_Components.Generate.generate import generate
from Pipeline_Components.Preprocess.preprocess import preprocess_new
from Pipeline_Components.Validate.validate import validate

kfp.components.func_to_container_op(input_parameters, base_image='python:3.7',output_component_file='Components_YAML/input.yaml')
kfp.components.func_to_container_op(model_parameters, base_image='gitlab-registry.cern.ch/ai-ml/kubeflow_images/tensorflow-notebook-gpu-2.1.0:v0.6.1-33',output_component_file='Components_YAML/model_para.yaml')
kfp.components.func_to_container_op(model_setup, base_image='gitlab-registry.cern.ch/gkohli/mlfastsim-kubeflow-pipeline/kube_gkohli:cern_pipelinev2',output_component_file='Components_YAML/model_setup.yaml')
kfp.components.func_to_container_op(preprocess_new, base_image='gitlab-registry.cern.ch/gkohli/mlfastsim-kubeflow-pipeline/kube_gkohli:cern_pipelinev2',output_component_file='Components_YAML/preprocess.yaml')
kfp.components.func_to_container_op(generate, base_image='gitlab-registry.cern.ch/gkohli/mlfastsim-kubeflow-pipeline/kube_gkohli:cern_pipelinev2',output_component_file='Components_YAML/generate.yaml')
kfp.components.func_to_container_op(validate, base_image='gitlab-registry.cern.ch/gkohli/mlfastsim-kubeflow-pipeline/kube_gkohli:cern_pipelinev2',output_component_file='Components_YAML/validate.yaml')










